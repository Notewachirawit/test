describe('As a customer, I want to add items into the cart and adjust my order detail in My cart page.', () => {
    it('Add product ', () => {
      cy.visit(' https://factools.qa.maqe.com/catalog',{timeout: 50000} )
      cy.viewport(1920, 1080)
      cy.get('[class="product-list-title"]',{timeout:5000})
     
      cy.xpath('//h2[text()="BOSNY VIT ผงเคมีอุดน้ำรั่ว B216"]').click('')
      cy.xpath('//input[@type="number"]').clear('').type('2')
      cy.get('[class="clect"]').scrollIntoView().click('')
      cy.xpath('//li[text()="เขียว"]').click({ force: true })
      cy.get('[data-test="add-to-cart"]').click('')
      cy.get('[data-test="cart-badge-amount"]').click('')
      
      // verify product price
       cy.get('[data-test="cart-price-subtotal"]').should('have.text', '360.00')
       // verify product detail 
       cy.get('[class="cart-item-subtitle"]').should('have.text', 'BOSNY VIT ผงเคมีอุดน้ำรั่ว B216 2ปอนด์ (กป.) : [สี เขียว]')
      // verify quantity product
      cy.get('[type="number"]').should('have.value','2')
      cy.wait(2000)
  
    })
    
    
    it('Add product to cart with invalid data', () => {
      cy.visit(' https://factools.qa.maqe.com/catalog',{timeout: 50000} )
      
      cy.get('[class="product-list-title"]',{timeout:5000})
      cy.xpath('//h2[text()="BOSNY VIT ผงเคมีอุดน้ำรั่ว B216"]').click('')
      
      var x
      var quantity = ['1.5','0','0.5','-3','100','105']
      for(x in quantity){
          cy.xpath('//input[@type="number"]').clear('').type(`${quantity[x]}`)
          cy.get('[class="clect"]').scrollIntoView().click('')
          cy.xpath('//li[text()="เขียว"]').click({ force: true })
          cy.get('[data-test="add-to-cart"]').click('')
  
      }
      cy.get('[data-test="cart-badge-amount"]').click({ force: true })
      cy.wait(1000)
    })


    it('The user adjust  order detail in cart page', () => {
        cy.visit(' https://factools.qa.maqe.com/catalog',{timeout: 50000} )
        cy.viewport(1920, 1080)
        cy.get('[class="product-list-title"]',{timeout:2000})
       
        cy.xpath('//h2[text()="BOSNY VIT ผงเคมีอุดน้ำรั่ว B216"]').click('')
        cy.xpath('//input[@type="number"]').clear('').type('2')
        cy.get('[class="clect"]').scrollIntoView().click('')
        cy.xpath('//li[text()="เขียว"]').click({ force: true })
        cy.get('[data-test="add-to-cart"]').click('')
        cy.get('[data-test="cart-badge-amount"]').click('')
        
        // verify product price
         cy.get('[data-test="cart-price-subtotal"]').should('have.text', '360.00')
         // verify product detail 
         cy.get('[class="cart-item-subtitle"]').should('have.text', 'BOSNY VIT ผงเคมีอุดน้ำรั่ว B216 2ปอนด์ (กป.) : [สี เขียว]')
        // verify quantity product
        cy.get('[type="number"]').should('have.value','2')

        // the user increase quantity product
        cy.get('[data-test="cart-product-sku-amount"]').clear('').type('10')
        cy.get('[class="cart-item-subtitle"]').click('')
        cy.get('[data-test="cart-price-subtotal"]').should('have.text', '1,800.00',{timeout:5000})
        cy.get('[type="number"]').should('have.value','10')
        // the user decrease quantity product
        cy.get('[data-test="cart-product-sku-amount"]').clear('').type('1')
        cy.get('[class="cart-item-subtitle"]').click('')
        cy.get('[data-test="cart-price-subtotal"]').should('have.text', '180.00')
        cy.get('[type="number"]').should('have.value','1')
        cy.get('[class="btn btn-expand js-go-checkout"]').click('')
        cy.wait(2000)
    
      })
      
      it('The user delete order detail in cart page', () => {
        cy.visit(' https://factools.qa.maqe.com/catalog',{timeout: 50000} )
        cy.viewport(1920, 1080)
        cy.get('[class="product-list-title"]',{timeout:5000})
       
        cy.xpath('//h2[text()="BOSNY VIT ผงเคมีอุดน้ำรั่ว B216"]').click('')
        cy.xpath('//input[@type="number"]').clear('').type('2')
        cy.get('[class="clect"]').scrollIntoView().click('')
        cy.xpath('//li[text()="เขียว"]').click({ force: true })
        cy.get('[data-test="add-to-cart"]').click('')
        cy.get('[data-test="cart-badge-amount"]').click('')
        
        // verify product price
         cy.get('[data-test="cart-price-subtotal"]').should('have.text', '360.00')
         // verify product detail 
         cy.get('[class="cart-item-subtitle"]').should('have.text', 'BOSNY VIT ผงเคมีอุดน้ำรั่ว B216 2ปอนด์ (กป.) : [สี เขียว]')
        // verify quantity product
        cy.get('[type="number"]').should('have.value','2')
        
        //delete product and verify should not have product in cart page
        cy.get('[data-test="cart-product-remove"]').click('')
        cy.get('[class="cart-item-subtitle"]').should('not.have.text', 'BOSNY VIT ผงเคมีอุดน้ำรั่ว B216 2ปอนด์ (กป.) : [สี เขียว]')

    
      })

      it('The user adjust  order detail with invalid data in cart page', () => {
        cy.visit(' https://factools.qa.maqe.com/catalog',{timeout: 50000} )
        cy.viewport(1920, 1080)
        cy.get('[class="product-list-title"]',{timeout:2000})
       
        cy.xpath('//h2[text()="BOSNY VIT ผงเคมีอุดน้ำรั่ว B216"]').click('')
        cy.xpath('//input[@type="number"]').clear('').type('2')
        cy.get('[class="clect"]').scrollIntoView().click('')
        cy.xpath('//li[text()="เขียว"]').click({ force: true })
        cy.get('[data-test="add-to-cart"]').click('')
        cy.get('[data-test="cart-badge-amount"]').click('')
        
        // verify product price
         cy.get('[data-test="cart-price-subtotal"]').should('have.text', '360.00')
         // verify product detail 
         cy.get('[class="cart-item-subtitle"]').should('have.text', 'BOSNY VIT ผงเคมีอุดน้ำรั่ว B216 2ปอนด์ (กป.) : [สี เขียว]')
        // verify quantity product
        cy.get('[type="number"]').should('have.value','2')

        // the user increase quantity product
        cy.get('[data-test="cart-product-sku-amount"]').clear('').type('101')
        cy.get('[class="cart-item-subtitle"]').click('')
        cy.get('[data-test="cart-price-subtotal"]').should('have.text', '18,180.00',{timeout:5000})
        cy.get('[type="number"]').should('have.value','101')
        
        cy.get('[class="btn btn-expand js-go-checkout"]').click('')
        cy.wait(2000)
    
      })
      it('Add product with button เลือกซื้อสินค้าต่อ', () => {
        cy.visit(' https://factools.qa.maqe.com/catalog',{timeout: 50000} )
        cy.viewport(1920, 1080)
        cy.get('[class="product-list-title"]',{timeout:5000})
       
        cy.xpath('//h2[text()="BOSNY VIT ผงเคมีอุดน้ำรั่ว B216"]').click('')
        cy.xpath('//input[@type="number"]').clear('').type('2')
        cy.get('[class="clect"]').scrollIntoView().click('')
        cy.xpath('//li[text()="เขียว"]').click({ force: true })
        cy.get('[data-test="add-to-cart"]').click('')
        cy.get('[data-test="cart-badge-amount"]').click('')
        
        // verify product price
         cy.get('[data-test="cart-price-subtotal"]').should('have.text', '360.00')
         // verify product detail 
         cy.get('[class="cart-item-subtitle"]').should('have.text', 'BOSNY VIT ผงเคมีอุดน้ำรั่ว B216 2ปอนด์ (กป.) : [สี เขียว]')
        // verify quantity product
        cy.get('[type="number"]').should('have.value','2')

        // add product with button "เลือกซื้อสินค้าต่อ"
        cy.get('[class="btn btn-ghost"]').click('')
        cy.get('[class="product-list-title"]',{timeout:5000})
        // direct to catalog page
        cy.xpath('//h2[text()="BOSNY VIT ผงเคมีอุดน้ำรั่ว B216"]').click('')
        cy.xpath('//input[@type="number"]').clear('').type('1')
        cy.get('[class="clect"]').scrollIntoView().click('')
        cy.xpath('//li[text()="เขียว"]').click({ force: true })
        cy.get('[data-test="add-to-cart"]').click('')
        cy.get('[data-test="cart-badge-amount"]').click('')
        // verify product price
        cy.get('[data-test="cart-price-subtotal"]').should('have.text', '540.00')
        // verify product detail 
        cy.get('[class="cart-item-subtitle"]').should('have.text', 'BOSNY VIT ผงเคมีอุดน้ำรั่ว B216 2ปอนด์ (กป.) : [สี เขียว]')
       // verify quantity product
       cy.get('[type="number"]').should('have.value','3')

        
    
      })
  })